function [] = writeFilePaths(filelist, txtpath)

    fid = fopen(txtpath, 'w+');
    
    for i = 1:1:numel(filelist)
        p = filelist{i};
        
        if i > 1
            fprintf(fid,'\n');
        end
        
        fprintf(fid, '%s',p);
    end
    
    fclose(fid);


end
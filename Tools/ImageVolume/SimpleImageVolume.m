classdef SimpleImageVolume
    % Encapsulates geometry information to position image voxels in space
    % 
    % This resource is excellent for summarizing DICOM geometry
    % http://nipy.org/nibabel/dicom/dicom_orientation.html
    properties 
        origin %(x, y, z) for voxelData(1,1,1);
        directionCosines % (row, col, slc)
        voxelSpacing %(row, col, slc)
        voxelData % (row, col, slc)
        dims
        center
        modality = 'MR'; % default value
        fov
        S % affine transformation matrix
        Sinv
    end
    
    methods
        
        function obj = ImageVolume(voxelData, ...
                                   origin, ...
                                   directionCosines, ...
                                   voxelSpacing, ...
                                   dims)
                               
            
            % Check # of slices matches header info
            
            allEqual = @(x,y) isequal(x(:),y(:));
            
            szVoxelData = size(voxelData);
            if ~isempty(voxelData) && ~allEqual(szVoxelData(1:3), dims(1:3)) 
                warning(['Dimensions do not match.', ...
                        sprintf(' Interpolating from [%i %i %i] to [%i %i %i]', ...
                                szVoxelData(1:3), dims(1:3))]);
                voxelData = obj.regridarray(voxelData, dims);
            end
            
            % Initialize params
            obj.voxelData = voxelData;
            obj.origin = origin;
            obj.directionCosines = directionCosines;
            obj.voxelSpacing = voxelSpacing;
            
            if ~isempty(voxelData)
                dims = size(voxelData);
            end
            
            obj.dims = dims;
            
            obj.S = zeros(4);
            obj.S(1:3,1:3) = bsxfun(@times, obj.voxelSpacing(:)', obj.directionCosines);
            obj.S(1:3,4)   = obj.origin(:);
            obj.S(4,4)     = 1;
            obj.Sinv = inv(obj.S);
        
        end
        
        
        function varargout = ind2coord(obj,r,c,s)
                       
            r = r-1; c = c-1; s = s-1; % convert to zero-based index
            S = obj.S;
            
            x = S(1,1).*r + S(1,2).*c + S(1,3).*s + S(1,4);
            y = S(2,1).*r + S(2,2).*c + S(2,3).*s + S(2,4);
            z = S(3,1).*r + S(3,2).*c + S(3,3).*s + S(3,4);
            
            if nargout < 2
                val.x = x;
                val.y = y;
                val.z = z;
                varargout{1} = val;
            else
                val = {x,y,z};
                for i = 1:1:nargout
                    varargout{i} = val{i};
                end
            end
            
        end
        
        function varargout = coord2ind(obj,x,y,z)
           
            [x0,y0,z0]   = ImageVolume.dealarr(obj.origin);
            [dr,dc,ds]   = ImageVolume.dealarr(obj.voxelSpacing);
            Sinv         = obj.Sinv;
            
            r = Sinv(1,1).*x + Sinv(1,2).*y + Sinv(1,3).*z + Sinv(1,4);
            c = Sinv(2,1).*x + Sinv(2,2).*y + Sinv(2,3).*z + Sinv(2,4);
            s = Sinv(3,1).*x + Sinv(3,2).*y + Sinv(3,3).*z + Sinv(3,4);

            r = r+1; c = c+1; s = s+1; %Convert back to MATLAB indexing
            
            if nargout < 2
                val.r = r;
                val.c = c;
                val.s = s;
                varargout{1} = val;
            else
                val = {r,c,s};
                for i = 1:1:nargout
                    varargout{i} = val{i};
                end
            end
            
        end
        
        function center = get.center(obj)
            dims = obj.dims;
            [x1,y1,z1] = ImageVolume.dealarr(obj.origin);
            [x2,y2,z2] = obj.ind2coord(obj.dims(1), dims(2), dims(3));
            
            center = 0.5*[x1;y1;z1] + 0.5*[x2;y2;z2];
            
        end
        
        function dims = get.dims(obj)    
            if ~isempty(obj.voxelData)
                dims = size(obj.voxelData);
            else
                dims = obj.dims;
            end  
        end
        
        function fov = get.fov(obj)
           
            fov = (obj.dims(:)).*obj.voxelSpacing(:);
            
        end
        
        function varargout = regridvolume(this, varargin)
           % this.regridvolume(n, dim) - upsamples dimension dim to n
           % points
           % this.regridvolume(refImageVolume) - regrids voxelData to
           % another ImageVolume 
           
           if  isnumeric(varargin{1})
               newdims = varargin{1}
               varargout{1} = regridarray(this.voxelData, newdims);
           else
               IVout = varargin{1};
               [voxelData, interpolator] = this.regridvolume2match(IVout);
               
               IVregridded = IVout;
               IVregridded.voxelData = voxelData;
               IVregridded.modality  = this.modality;
               varargout{1} = IVregridded; varargout{2} = interpolator;
           end
              
        end
        
        function [voxelData, interpolator] = regridvolume2match(this, IVout)
            % This function lets you regrid imstackIn so that it is spatially
            % registered to the image volume with dicom header hdrOut and size dims.

            % There are 3 different coordinate systems (source, world, destination)
            % The goal is to expression imstackIn in destination coordinates

                [nRow, nCol, nSlice] = ImageVolume.dealarr(this.dims);
                [J, I, K]   =  meshgrid(1:nCol, 1:nRow, 1:nSlice);
                [X, Y, Z]   =  this.ind2coord(I, J, K);

                % 1) Compute output grid in world coordinates
                [nRow, nCol, nSlice] = ImageVolume.dealarr(IVout.dims);
                [Jtemp, Itemp, Ktemp]   =  meshgrid(1:nCol, 1:nRow, 1:nSlice);
                [Xq, Yq, Zq]   =  IVout.ind2coord(Itemp, Jtemp, Ktemp);

                % 2) Compute output grid in source coordinates
                [Iq, Jq, Kq] = this.coord2ind(Xq, Yq, Zq);

                % 3) Perform Interpolation
                %     - interpolate values of input image at output grid points
                
             
                interpolator = @(x) interp3(J, I, K, x, Jq, Iq, Kq, 'linear', 0);
                voxelData    = interpolator(this.voxelData);

        end
        
        function [] = writeDicomStack(obj, dirPath)
           
            modality = obj.modality;
            
            col = @(x) x(:);
            
            info = ImageVolume.getDefaultTags();
            nSlice = obj.dims(end);
            
            [parentDir, dirName, ext] = fileparts(dirPath);
            mkdir(dirPath);
            
            if ~isempty(modality)
                info.Modality = ImageVolume.disambiguateModality(modality);
            end
                    
           
            % Read Volume data
            disp('Writing Dicom Files...');
            voxelData = im2uint16(mat2gray(abs(obj.voxelData)));
            for iSlice = 1:size(obj.voxelData,3)
                
                im = obj.voxelData(:, :, iSlice);
                [Px,Py,Pz] = obj.ind2coord(1, 1, iSlice);
                IOP = col(obj.directionCosines(:, [2,1]));
                IPP = col([Px, Py, Pz]);
                n   = cross(IOP(1:3,:),IOP(4:6,:));
                SliceLocation  = dot(n,IPP);          

                info.Width                = uint16(obj.dims(2));
                info.Height               = uint16(obj.dims(1));
                info.PixelSpacing         = obj.voxelSpacing(1:2);
                info.SliceThickness       = obj.voxelSpacing(end);
                info.ImageOrientationPatient = IOP;
                info.ImagePositionPatient = IPP;
                info.SliceLocation        = SliceLocation;
                info.InstanceNumber       = uint16(iSlice);
                
                filePath = sprintf('%s/%s_%03i.dcm',fullfile(parentDir, dirName), ... 
                                   dirName, iSlice);
                
                dicomwrite(voxelData(:,:,iSlice), filePath, info, 'createMode', 'copy') ;
                       
            end
            
        end    
              
    end
    
    methods(Static)
        function varargout = dealarr(x)
            for i = 1:1:numel(x)
               varargout{i} = x(i); 
            end
        end
        
        function numstr=number2string(num)
            num=num2str(num);
            numzeros='000000';  
            numstr=[numzeros(length(num):end) num];            
        end
        
        function info = getDefaultTags()
            % Add dicom tags to info structure
            info=struct;
            % Make random series number
            SN=round(rand(1)*1000);
            % Get date of today
            today=[datestr(now,'yyyy') datestr(now,'mm') datestr(now,'dd')];
            info.SeriesNumber=SN;
            info.AcquisitionNumber=SN;
            info.StudyDate=today;
            info.StudyID=num2str(SN);
            info.PatientID=num2str(SN);
            info.PatientPosition='HFS';
            info.AccessionNumber=num2str(SN);
            info.StudyDescription=['StudyMAT' num2str(SN)];
            info.SeriesDescription=['StudyMAT' num2str(SN)];
            info.Manufacturer='Matlab Convert';
            info.SOPClassUID = dicomuid;
            info.StudyInstanceUID = dicomuid;
            info.SeriesInstanceUID = dicomuid;
        end
        
        function modality = disambiguateModality(modality)
           switch upper(modality)
                case 'PT'
                    modality = 'PT';
                case 'PET'
                    modality = 'PT';
                case 'MR'
                    modality = 'MR';
                case 'MRI'
                    modality = 'MR';
           end 
        end
        
        function Xnew = regridarray(X, newDims)
            %REGRIDARRAY interpolate array to new dimensions
            %
            %   regridarray(X, newDims) interpolates array X to newDims, where newDims
            %   is an array, such that size(regridarray(X, newDims)) = newDims
            %
            %   newDims = [256, -1, 256] means keep the second dimension the same


                oldDims = size(X);
                idx          = (newDims == -1);
                newDims(idx) = oldDims(idx);

                if newDims ~= round(newDims)
                    warning('rounding dimensions to integer values');
                    newDims = round(newDims);
                end

                if ~isreal(X)
                    disp('Regridding magnitude and phase separately');
                    R = regridarray(abs(X), newDims);
                    theta = regridarray(angle(X), newDims);
                    Xnew = R.*exp(i*theta);
                    return;
                end


                for iDim = 1:1:ndims(X)
                    gridvectors{iDim} = linspace(0, 1, oldDims(iDim));
                end

                F = griddedInterpolant(gridvectors, X);

                for iDim = 1:1:numel(newDims)
                   gridvectors{iDim} = linspace(0, 1, newDims(iDim));        
                end

                Xnew = F(gridvectors); 

       end
        
    end
    
    
end
    



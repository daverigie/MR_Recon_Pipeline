classdef Grad < LinOp
    
    % This implementation is kind of sloppy and involves permuting the 
    % dimensions of the array. This was the only obvious way to make it
    % work on arrays of arbitrary dimension, but it may need to be improved
    % later.
    
    properties
       dimArr = [4];
       dxArr
    end
    
    methods
        
        function obj = Grad(imageDims, dimArr, dxArr)
             
            if nargin > 1
                obj.dimArr = dimArr;
            end
            
            if nargin < 3
               dxArr = ones([1,numel(dimArr)]); 
            end
            
            obj.dxArr = dxArr;
            obj.inputShape = imageDims;
            
            if numel(dimArr) == 1
                obj.outputShape = imageDims;
            else
                obj.outputShape = [imageDims, numel(dimArr)];
            end
        end
       
        function y = mtimesForward(obj,x)
            
            y = gradNd(x,obj.dimArr, obj.dxArr);
           
        end
        
        function y = mtimesAdj(obj, x)
           
            y = -divNd(x, obj.dimArr, obj.dxArr);
           
        end
        
    end
    
end


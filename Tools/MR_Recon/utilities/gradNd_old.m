function y = gradNd_old(x, dimArr)
% Find an n-dimensional gradient by sequentially taking finite differences
% along the dimensions in dimArr. 

    nDim = numel(dimArr);
    
    if nDim == 1
        y = grad1d_old(x,dimArr(1));
    else
    
        expandDim = ndims(x) + 1;

        y = [];

        for i=1:nDim
           dim = dimArr(i);
           y = cat(expandDim, y, grad1d_old(x,dim));
        end

    end
    
end
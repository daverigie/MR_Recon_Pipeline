function y = divNd(x, dimArr, dxArr)
% Find an n-dimensional gradient by sequentially taking finite differences
% along the dimensions in dimArr. 

    nDim = numel(dimArr);
    
    if nargin < 3
        dxArr = ones([1,nDim]);
    end
    
    if nDim == 1
        y = div1d(x,dimArr(1), dxArr(1));
    else
        
    

        dimX = ndims(x);
        szX = size(x);
        szY = szX(1:(end-1));
        
        y = zeros(szY);

        for i=1:nDim
           dim = dimArr(i);
           dx  = dxArr(i);
           ind = [repmat({':'},[1,(dimX-1)]), i];
           y   = y + div1d(x(ind{:}), dim, dx);
        end

    end
    
end
function y = gradNd(x, dimArr, dxArr)
% Find an n-dimensional gradient by sequentially taking finite differences
% along the dimensions in dimArr. 

    nDim = numel(dimArr);
    
    if nargin < 3
        dxArr = ones([1,nDim]);
    end
    
    if nDim == 1
        y = grad1d(x,dimArr(1), dxArr(1));
    else
    
        expandDim = ndims(x) + 1;

        y = [];

        for i=1:nDim
           dim = dimArr(i);
           dx  = dxArr(i);
           y = cat(expandDim, y, grad1d(x,dim, dx));
        end

    end
    
end
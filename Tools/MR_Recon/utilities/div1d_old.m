function y = div1d(x, dim)

    % Shift dimensions to operate on first one
    N = size(x,dim);
    [x, perm, nshifts] = shiftdata(x, dim);
    y = zeros(size(x));

    y(:,:)    =  x([1,1:(N-1)], :) - x(1:N, :); 
    y(1,:)    = -x(1,:);
    y(N,:)    = x(N-1,:);

    % shift back
    y = -1.0*unshiftdata(y, perm, nshifts);

end


function y = divNd_old(x, dimArr)
% Find an n-dimensional gradient by sequentially taking finite differences
% along the dimensions in dimArr. 

    nDim = numel(dimArr);
    
    if nDim == 1
        y = div1d_old(x,dimArr(1));
    else

        dimX = ndims(x);
        szX = size(x);
        szY = szX(1:(end-1));
        
        y = zeros(szY);

        for i=1:nDim
           dim = dimArr(i);
           ind = [repmat({':'},[1,(dimX-1)]), i];
           y   = y + div1d_old(x(ind{:}), dim);
        end

    end
    
end
classdef gpuNUFFTND < LinOp
    % This class provides a generic wrapper for gpuNUFFT that acts like a
    % linear operator and handles data with 3 spatial dimensions plus 1 or
    % 2 (and possibly arbitrary but not tested) extra dimensions, such as
    % time, or card/resp phase
    
        
    properties %(Access = private)
        gpuNUFFTArr
        densCompenArr
        densityCompensationMode = 'symmetric';
        %densityCompensationMode = 'asymmetric'; 
    end
    
    properties (SetAccess = immutable)
       nGate 
    end
        
    methods
        
        % Constructor
        function obj = gpuNUFFTND(trajCellArr, densCompenCellArr, sens4d, imageDim, ... 
                                    osf, wg, sw, varargin)
            
            nCoil = size(sens4d,4);
                              
            obj.nGate = numel(trajCellArr);
            
            obj.gpuNUFFTArr = cell(size(trajCellArr));
            obj.outputShape = cell(size(trajCellArr));
            obj.densCompenArr = densCompenCellArr;
            
            %______________________________________________________________
            % Globally scale density compensation for each gate to account
            % for uneven data distribution. This is important when
            % different gates have different numbers of spokes etc. 
            sum1d          = @(x) sum(x(:));
            densSums       = cellfun(sum1d, obj.densCompenArr);
            maxSum         = max(densSums);
            scaleFactors   = bsxfun(@rdivide, maxSum, densSums);;
    
            for i = 1:1:numel(obj.densCompenArr)
               obj.densCompenArr{i} = obj.densCompenArr{i}*scaleFactors(i); 
            end
            %_____________________________________________________________%
            
            for i = 1:1:obj.nGate
            
                traj       = trajCellArr{i};
                densCompen = ones(size(densCompenCellArr{i})); % density compensation is applied outside of gpuNUFFT so that we can toggle whether it is applied symmetrically or not
                sens       = sens4d;
                
                if nargin < 5
                    % These parameters impact the interpolation in the
                    % NUFFT. This default choice seems to provide a good
                    % balance of speed and accuracy.
                    osf = 2;  wg = 3;  sw = 8;
                end
                
                if nargin < 4
                    imageDim = size(sens);
                    imageDim = imageDim(1:3);
                end
                
                temp = gpuNUFFT(traj, densCompen, ...
                                osf, wg, sw, ... 
                                imageDim, sens, varargin{:});
                                   
                obj.gpuNUFFTArr{i} = temp;
                
                
                dDims = size(densCompen);
                if dDims(end) == 1
                    dDims = dDims(1:(end-1));
                end
                
                obj.outputShape{i}  = [dDims, nCoil]; 
            
                
            end
            
            obj.inputShape = [imageDim, size(obj.outputShape)];
            

        end
        
        
        function v = vec(obj, data)
        % Flatten data into 1D column vector shape. Works for nested cell
        % arrays of matrices as well
            mat2vec = @(m) m(:);

            if iscell(data)
                data = cell2mat(cellfun(mat2vec, data(:), 'UniformOutput', false));
            end

            v = data(:);  
        end
        
        function data = restore(obj, v)
        % Restores data back to its natural shape
        
            if ~obj.adj
               data = reshape(v, obj.inputShape); 
            else
               data = obj.restoreData(v);  
            end
        end
        
        function data = restoreData(obj, v)
            % Convert column vector back to natural kdata shape
            adjFlagVal = obj.adj;
            obj.adj = true;
            obj.checkValidSize(v);
                
            data = cell(size(obj.outputShape));

            imin = 1;
            imax = -1;
            
            for i = 1:1:obj.nGate
                temp = zeros(obj.outputShape{i}, 'like', v(1));
                chunkSize = numel(temp);
                imin = max(imin, imax + 1);
                imax = imin + chunkSize - 1;
                temp(:) = v(imin:imax);
                data{i} = temp;
            end
            
            obj.adj = adjFlagVal;
        end
        
        function x = scaleData(obj, x)
            % When using the NUFFT in symmetric mode for iterative
            % reconstruction, the data should usually be prescaled by the
            % sqrt of the densityCompensation.
            
            if isnumeric(x) && isvector(x)
               x = obj.restoreData(x(:)); 
            end
            
            for i = 1:1:obj.nGate
                x{i} = bsxfun(@times, x{i}, sqrt(obj.densCompenArr{i}));
            end
            
            if obj.flatVectors
                x = obj.vec(x);
            end
        end
        
        function val = mtimes(obj,x)
            val = mtimes@LinOp(obj,x)/10.0;
        end
        
        function val = mtimesForward(obj, x)
           val = cell(size(obj.outputShape));
               
           for i = 1:1:obj.nGate
              FT = obj.gpuNUFFTArr{i};
              val{i} = FT*x(:,:,:,i); 
              
              switch upper(obj.densityCompensationMode)
                  case 'SYMMETRIC'
                      val{i} = bsxfun(@times, val{i}, sqrt(obj.densCompenArr{i}));
                  case 'ASYMMETRIC'
                      ;
                  otherwise
                      error('invalid density compensation mode. Must be symmetric or asymmetric');
              end
              
           end
        end
  
        function val = mtimesAdj(obj, x)
            val = zeros(obj.inputShape, 'like', x{1});
                
            for i = 1:1:obj.nGate
                FT = obj.gpuNUFFTArr{i};      
  
                switch upper(obj.densityCompensationMode)
                  case 'SYMMETRIC'
                      x{i} = bsxfun(@times, x{i}, sqrt(obj.densCompenArr{i}));
                  case 'ASYMMETRIC'
                      x{i} = bsxfun(@times, x{i}, obj.densCompenArr{i}); 
                  otherwise
                      error('invalid density compensation mode. Must be symmetric or asymmetric');
              end
                
                val(:,:,:,i) = FT'*x{i};
            end
        end
        
        function tf = checkValidSize(obj, x)
             if obj.adj
                 outputShape = cell2mat(obj.outputShape(:));
                 validSize = sum(prod(outputShape,2));
             else
                 validSize = prod(obj.inputShape);
             end
             
             assert(numel(obj.vec(x)) == validSize, 'Invalid input size');
             
             tf = true;
              
        end
        
        function eps = checkOperatorAdjoint(A, x, y)    
            
            if nargin < 2
               x = complex(rand(A.inputShape), rand(A.inputShape));
               y = A*x;
               y = complex(rand(size(y)),rand(size(y)));
            end
            
            eps = checkOperatorAdjoint@LinOp(A, x, y);

        end
        
    end
    

            
        
        
        
        
end
    


classdef (Abstract) LinOp 
       
    properties
        adj@logical = false
        flatVectors@logical = true    
    end
    
    properties (SetAccess = protected)
        inputShape
        outputShape  
    end
    
    methods
        
        function obj = Grad3d1d(inputShape, outputShape)
            obj.inputShape = inputShape;
            obj.outputShape = outputShape;
        end
        
        function y = vec(obj,x)
            y = x(:);
        end
        
        % Check adjoint flag
        function obj = ctranspose(obj)
            obj.adj = xor(obj.adj, 1);
        end
        
        function y = restore(obj,x)
            
           obj.checkValidSize(x);
            
           if obj.adj
               y = reshape(x, obj.outputShape);
           else
               y = reshape(x, obj.inputShape);
           end
        end

        function y = mtimes(obj, x)
            
            if isnumeric(x) && isvector(x)
               x = obj.restore(x(:)); 
            end
            
            obj.checkValidSize(x);
            if obj.adj
                y = obj.mtimesAdj(x);
            else
                y = obj.mtimesForward(x);
            end
            
            if obj.flatVectors
                y = obj.vec(y);
            end
        end
        
        function tf = checkValidSize(obj, x)
            
             if obj.adj
                 validSize = prod(obj.outputShape);
             else
                 validSize = prod(obj.inputShape);
             end
             
             assert(numel(obj.vec(x)) == validSize, 'invalid input size')             
             
             tf = true; 
        end
        
        function eps = checkOperatorAdjoint(A, x, y)

            fprintf('\n');
            
            if nargin < 2
               x = rand(A.inputShape);
               y = A*x;
               y = rand(A.outputShape);
            end

            A.flatVectors = true;

            Axy = dot(A*x,y(:));
            xATy = dot(x(:),A'*y);

            eps = abs(Axy-xATy)/abs(Axy);

            fprintf('\n<Ax,y> = %f + %fi', real(Axy), imag(Axy));
            fprintf('\n<x,A^Hy> = %f + %fi', real(xATy), imag(xATy));
            fprintf('\n%% difference is %10E', eps);
            
            fprintf('\n');

        end
        
    end
    
    methods(Abstract)
        y = mtimesForward(obj, x);
        y = mtimesAdj(obj, x);
    end
    
end


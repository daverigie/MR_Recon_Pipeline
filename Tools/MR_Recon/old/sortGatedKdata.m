function [kdata4d, traj4d, densityCompen4d] = sortGatedKdata(kdata, ...
                                                             traj, ...
                                                             densityCompen, ...
                                                             gatingSignal)
    
    [nCol, nPar, nView, nCoil] = size(kdata);
    
    gatingSignal = col(gatingSignal);
    nGates = numel(unique(gatingSignal)); 
    
    % Determines how many lines are assigned to each gate
    gatingSignalHist = histcounts(gatingSignal, nGates, 'BinMethod', 'integers');
    minChunkSize = min(gatingSignalHist); % Find the amount of data in the motion state with least data.

    function data4d = binByGateNum(data)
    % Sort data by gate number and create an extra "time" dimension
    
        data   = reshape(data, nCol, nPar*nView, []);
        data4d = zeros([nCol, minChunkSize, size(data,3), nGates]);
        
        for iGate = 1:nGates
            ind = find(gatingSignal == iGate);
            ind = ind(1:minChunkSize);
            %size(data(:,ind,:)) !DEBUG!
            %size(data4d(:,:,:,iGate)) !DEBUG!
            data4d(:,:,:,iGate) = data(:, ind, :);
        end
        
        data4d = squeeze(reshape(data4d, nCol*minChunkSize, [], nGates));
        
    end
    
    kdata4d         = single(binByGateNum(kdata));
    traj4d          = single(binByGateNum(traj));
    densityCompen4d = single(binByGateNum(densityCompen));

end



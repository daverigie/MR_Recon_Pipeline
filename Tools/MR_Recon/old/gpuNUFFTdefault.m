function obj = gpuNUFFTdefault(traj, densCompen, imageDim, sens, varargin)

    OSF = 1.5; % Oversampling factor
    WG  = 3;   % Kernel Width
    SW  = 8;   % Sector width

    if nargin < 4
        sens = [];
    end
    
    if ~isempty(sens)
        imageDim = size(sens);
        imageDim = imageDim(1:(end-1));
    end
    
    obj =     gpuNUFFT(traj, densCompen, ... 
                       OSF, WG, SW, ... 
                       imageDim, sens, ...
                       varargin{:});
end


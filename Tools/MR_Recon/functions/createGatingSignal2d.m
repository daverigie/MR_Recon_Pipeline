function [new_gating_signal] = createGatingSignal2d(sig2d, nGates)

    % size(sig2d)  = [N,2]
    % size(nGates) = [1,2]

    if size(sig2d,2) > size(sig2d,1)
        warning('2d gating signal matrix oriented wrong, transposing');
        sig2d = sig2d.';
    end

    sig1 = sig2d(:,1);
    sig2 = sig2d(:,2);
    
    nGates1 = nGates(1);
    nGates2 = nGates(2);

    % Takes two raw gating signals and turns them into one long gating
    % signal with nGates1*nGates2
    
    new_gating_signal_1 = createGatingSignal1d(sig1, nGates1);
    new_gating_signal = zeros([length(sig1), 2]);
    new_gating_signal(:) = nan;
    new_gating_signal(:,1) = new_gating_signal_1;
    
    for i = 1:1:nGates1
       
        ind_i = find(new_gating_signal_1 == i);
        
        sig2_part = sig2(ind_i);
        
        new_gating_signal(ind_i,2) = createGatingSignal1d(sig2_part, nGates2); 
                                     
        
    end
    new_gating_signal(isnan(new_gating_signal)) = -1;


end


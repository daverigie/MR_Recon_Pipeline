function b1 = estimateCoilSensitivityMapsMod(kdata, traj3d, densityCompen3d, imageDim)

    BETA = 20;

    [nCol, nPar, nView, nCoil] = size(kdata);
    
    % Filter kdata
    windowFun = kaiser(nCol, BETA);
    kdata     = bsxfun(@times, kdata, windowFun);

    FT = gpuNUFFTdefault(traj3d.', densityCompen3d, imageDim);
    
    ref = zeros([imageDim(:)', nCoil]);
    
    % Reconstruct each coil separately using normal gridded recon
    for ch=1:nCoil
        ref(:,:,:,ch)= FT'*col(kdata(:,:,:,ch));
    end
    
    ref=single(ref/max(abs(ref(:))));

    % Calculate coil sensitivity maps
    b1=adapt_array_3d(ref);
    b1=single(b1/max(abs(b1(:))));


end
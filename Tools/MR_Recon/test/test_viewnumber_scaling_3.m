%% Load Data File
clear; clc;
load('Q:/labspace/MR_Recon_Dave_v2/sampleData/meas_MID03068_FID282426_Abd_RadVibeNyuNavi_082016_kdata_clean.mat');


%% Estimate Sensitivity Maps based on full kdata

kdata = kdata_clean;
imageDim = [256,256,48];
imones = ones(imageDim);
[nCol, nPar, nView, nCoil]  =   size(kdata);                
nSlice = imageDim(3);

%Generate the sampling trajectory
[traj3d, densityCompen3d] = goldenAngleStackOfStars(nCol, nSlice, nView, nPar);

b1 = estimateCoilSensitivityMaps(kdata, traj3d, densityCompen3d, imageDim);


%% Perform reconstructions with different numbers of views
imarr = cell(0);
viewArr = [50, 100, 200, 400, 800];
for numViews = viewArr
       
    kdata = kdata_clean(:,:,1:numViews,:);
    [nCol, nPar, nView, nCoil]  =   size(kdata);                
    nSlice = imageDim(3);

    %Generate the sampling trajectory
    [traj3d, densityCompen3d] = goldenAngleStackOfStars(nCol, nSlice, nView, nPar);

    FT    = gpuNUFFTND({traj3d.'}, {densityCompen3d}, b1);
    FT.densityCompensationMode = 'asymmetric';
    
    % Apply adjoint NUFFT to filtered/densitycomp data
    kdata = {reshape(kdata, [], nCoil)};
    
    im = FT'*kdata;
    im = FT.restore(im);
    imarr = [imarr, abs(im)];
    
end


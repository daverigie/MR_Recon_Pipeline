%% ________________________________________________________________________
%                        Creates NUFFT Operator                         
%__________________________________________________________________________

    clc; clear;

OVERSAMPLING_FACTOR = 2;
KERNEL_WIDTH = 3;
SECTOR_WIDTH = 8;

IMAGE_DIM = [64,64,48];

    
x = rand(IMAGE_DIM); % 64^3 image with 5 resp & 5 card gates
x = complex(rand(size(x)), rand(size(x)));
imageDim = size(x);
nSlice = imageDim(3);

nFE  = 64;
nPar  = 37;
nView = 100;
nCoil = 1;

kdataSize = [nFE, nPar, nView, nCoil];
y = complex(rand(kdataSize), rand(kdataSize));
y = y(:);

[traj, dens] = goldenAngleStackOfStars(nFE, nSlice, nView, nPar);


sensMap = ones([IMAGE_DIM, nCoil]);

A = gpuNUFFT(traj', dens*0.0+1.0, OVERSAMPLING_FACTOR, ... 
                                 KERNEL_WIDTH, ...
                                 SECTOR_WIDTH, ...
                                 IMAGE_DIM, ...
                                 [],true);
    
        
    
%==========================================================================

%% ________________________________________________________________________
%                           Check Operator Adjoint                         
%__________________________________________________________________________

vdot = @(x,y) dot(x(:),y(:));

vdot(A*x,y)
vdot(x,A'*y)


%==========================================================================
function time_milliseconds = getTimestampMilliseconds(twixobj)

    rescale = @(x, scalefactor) (x-min(x))*scalefactor;
    
    time_milliseconds = rescale(twixobj.image.pmutime, ...
                                milliseconds_per_clocktick_mMR());

    
end
function twix_obj = orderDataByTime(twix_obj)

    fprintf('\nChecking partition ordering...');
    
    imagehdr = twix_obj.image;
    
    idx = getPartitionSortingIdx(imagehdr);
    
    if issorted(idx)
        fprintf('\nNo correction needed.');
    else
       twix_obj.data = reorderData(twix_obj.data, idx);
       fprintf('\nData reordered according to acquisition order');
    end
        
    fprintf('\n\n');
        
end

function idx = getPartitionSortingIdx(imagehdr)
    
    nLin  = imagehdr.NLin;
    nPar  = imagehdr.NPar;
    nAcq  = imagehdr.NAcq;
    % Separate MDH into navigator and image data
    
    kdata.Par = imagehdr.Par(1:2:end);
    kdata.Lin = imagehdr.Lin(1:2:end);
    step = floor(nAcq/(nLin*nPar));
    kdata.timestamp = imagehdr.timestamp(1:step:end);

    % Find beginning after prep-pulses
    nPrepPulses = numel(kdata.Par) - nLin*nPar;
    fprintf('\nNumber of prep pulses: %d', nPrepPulses);

    kdata.Par = kdata.Par(nPrepPulses+1:end);
    kdata.Lin = kdata.Lin(nPrepPulses+1:end);
    kdata.timestamp = kdata.timestamp((nPrepPulses+1):end);

    I = kdata.Par + (kdata.Lin-1)*nPar;
    % Sort

    [~,idx_rev] = sort(I);
    [~,idx]     = sort(idx_rev);

end

function data = reorderData(data, parIdx)

    idx = parIdx;
    perm_ind = [1,4,2,3]; % put coil dimension before Par/View
    
    data = permute(data, perm_ind);
    data(:,:,:) = data(:,:,idx);
    data = ipermute(data, perm_ind);
    
end
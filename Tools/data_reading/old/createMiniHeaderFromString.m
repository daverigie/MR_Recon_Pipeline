function [head] = createMiniHeaderFromString(headerString)

    function paramValue =  search(searchterm, defaultVal)
    
        fprintf('query: %s ... ', searchterm);
        paramValue = searchHeaderString(headerString, searchterm);

        if nargin > 1
            if isempty(paramValue)
                paramValue = defaultVal;
            end
        end

        fprintf('found value(s): %s\n', num2str(paramValue));
    
    end
       
    head.osfactor              = search('osfactor'); % oversampling
    head.centerFreq            = search('lFrequency'); %Larmor Freq
    
    head.fov.read              = search('readfov');
    head.fov.phase             = search('phasefov');
    head.fov.slice             = search('Volume.dThickness');
    
    head.kdata.dwellTime       = search('dwellTime'); %nanoseconds
    
    head.kdata.dims.nCol       = search('rawCol');
    head.kdata.dims.nLin       = search('rawLin'); 
    head.kdata.dims.nPar       = search('NParMeas'); % number of partitions
    head.kdata.dims.nCha       = search('nCha'); % Number of coils
    head.scanTime              = search('scanTime'); % seconds
    head.TE                    = search('alTe');
    head.TR                    = search('alTR');
    head.lPartitions           = search('lPartitions');
    head.nImagesPerSlab        = search('ImagesPerSlab'); 
    
    head.offset.dSag           = search('sPosition.dsag', 0); % These are mm
    head.offset.dCor           = search('sPosition.dCor', 0);
    head.offset.dTra           = search('sPosition.dTra', 0);
    
    head.normalVector.dSag     = search('sNormal.dsag', 0);
    head.normalVector.dCor     = search('sNormal.dCor', 0);
    head.normalVector.dTra     = search('sNormal.dTra', 0);
  
    head.globalTablePos.dSag     = search('GlobalTablePosSag', 0);
    head.globalTablePos.dCor     = search('GlobalTablePosCor', 0);
    head.globalTablePos.dTra     = search('GlobalTablePosTra', 0);
    
    head.ImageSize.nRow           = search('NImageLins',      -1);
    head.ImageSize.nCol           = search('NImageCols',      -1);
    head.ImageSize.nSlice         = search('NoImagesPerSlab', -1);
        
    head.navigator.dwellTime   = 2500; % This is valid for Rosette as of 9/29/2016
    
end






function [] = printMiniHeader(twixpath)

    if nargin < 1
        twixpath = uigetpath('*.dat');
    end
    
    hdr = createMiniHeader(twixpath, false);
    
    s = printnestedstruct(hdr);

    fprintf('\n\n%s\n\n', s);

end